import React, { useEffect } from 'react';
import { ContainerFluid, Row } from '@uprise/grid';
import { Button } from '@uprise/button';
import { useLocation, useHistory } from 'react-router-dom';
import withAuth from 'components/withAuth';

const Connect = () => {
  const loc = useLocation(),
    his = useHistory();

  useEffect(() => {
    if (loc.hash) {
      console.log(loc);
      const hashObj = new URLSearchParams(loc.hash.replace('#', '?')),
        accessToken = hashObj.get('access_token');
      if (accessToken) {
        localStorage.setItem('up_cl_access_token', accessToken);
        return his.push('/overview');
      }
      console.error('error occured', hashObj.get('error'));
    }
  }, [loc, his]);

  const getUrl = () => {
    const {
      REACT_APP_BASE_URL,
      REACT_APP_CLIENT_ID,
      REACT_APP_RED_URI,
    } = process.env;
    return `${REACT_APP_BASE_URL}/authorize?client_id=${REACT_APP_CLIENT_ID}&response_type=token&redirect_uri=${REACT_APP_RED_URI}`;
  };

  return (
    <ContainerFluid className='mt-4'>
      <Row>
        <div className='col-6 offset-3'>
          <a className='connect-link' href={getUrl()}>
            <Button title='Connect to Spotify'></Button>
          </a>
        </div>
      </Row>
    </ContainerFluid>
  );
};

export default withAuth(Connect, true);
