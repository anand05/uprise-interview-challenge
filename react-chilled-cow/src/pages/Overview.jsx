import React, { useEffect, useContext } from 'react';
import { Container, Row, Col } from '@uprise/grid';
import { H2 } from '@uprise/headings';
import { Medium } from '@uprise/text';
import { Button } from '@uprise/button';
import { Card } from '@uprise/card';
import withAuth from 'components/withAuth';
import { getData } from 'utils/spotifyApi';
import authContext from 'context/auth/authContext';
import dataContext from 'context/data/dataContext';

const Overview = ({ bgColor }) => {
  const { getToken, deleteToken } = useContext(authContext),
    { delData, setData, data } = useContext(dataContext);

  useEffect(() => {
    getData(getToken()).then(({ errors, data: { user } }) => {
      console.log('Overview -> d', user);
      if (errors) {
        // token expire
        delData();
        return deleteToken();
      }
      setData(user);
    });
    // eslint-disable-next-line
  }, [getToken, deleteToken]);

  const getImage = () => {
    const { images = [null], display_name } = data;
    const [first] = images;
    return <img width='100%' src={first && first.url} alt={display_name} />;
  };

  return (
    <Card backgroundColor={bgColor} className='card-shadow mt-4 pd-4 pd-md-0'>
      <Container>
        <Row>
          <Col className='col-sm-6 col-md-5'>
            <div className='over-img'>{data && getImage()}</div>
          </Col>
          <Col className='col-sm-6 col-md-7 user-info'>
            <H2 weight='false' className='my-0'>
              {data && data.display_name}
            </H2>
            <Medium className='my-1'>Followers (124,234)</Medium>
            <a
              rel='noopener noreferrer'
              href={data && data.href}
              target='_blank'
            >
              <Button
                className='btm-follow'
                size='small'
                width='7rem'
                title='Follow'
              ></Button>
            </a>
          </Col>
        </Row>
      </Container>
    </Card>
  );
};

export default withAuth(Overview);
