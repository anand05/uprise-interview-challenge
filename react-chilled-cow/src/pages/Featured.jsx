import React, { useContext, useEffect } from 'react';
import { Card } from '@uprise/card';
import { ContainerFluid, Row, Col } from '@uprise/grid';
import { Button } from '@uprise/button';
import withAuth from 'components/withAuth';
import dataContext from 'context/data/dataContext';

const Featured = ({ bgColor }) => {
  const { data, getData } = useContext(dataContext);

  useEffect(() => {
    getData();
  }, []);

  return (
    <ContainerFluid className='mt-1'>
      <Row>
        <Col className='col-sm-1 col-lg-1 offset-sm-4 offset-lg-2'>
          <Card backgroundColor={bgColor} className='featured-card'>
            {data &&
              data.randomArts.map((t) => (
                <Button
                  key={t}
                  className='feat-list-btn'
                  variant='text'
                  title={t}
                ></Button>
              ))}
          </Card>
        </Col>
      </Row>
    </ContainerFluid>
  );
};

export default withAuth(Featured);
