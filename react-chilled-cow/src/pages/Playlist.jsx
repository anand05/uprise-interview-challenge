import React, { useContext, useEffect, useState } from 'react';
import { Container, Row, Col } from '@uprise/grid';
import { H3 } from '@uprise/headings';
import Icons from '@uprise/icons';
import { Card } from '@uprise/card';
import withAuth from 'components/withAuth';
import dataContext from 'context/data/dataContext';

const Playlist = ({ bgColor }) => {
  const [curPlaylist, setCurPlaylist] = useState(null),
    { getData, data } = useContext(dataContext);

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (!data) {
      return;
    }
    const { playlists = [null] } = data,
      [currentPlay] = playlists;
    setCurPlaylist(currentPlay);
    console.log('Playlist -> currentPlay', data);
  }, [getData]);

  const getImage = (plist) => {
    if (!plist) {
      return;
    }
    const {
      images: [first],
      name,
    } = plist;
    return <img width='100%' src={first.url} alt={name} />;
  };

  const nextPlaylist = (prev = false) => {
    if (!curPlaylist) {
      return;
    }
    const { playlists } = data,
      curPlIndex = playlists.findIndex((v) => v.id === curPlaylist.id);
    let newIndex = curPlIndex;
    if (!prev) {
      newIndex++;
    } else {
      newIndex--;
    }

    if (newIndex > playlists.length - 1) {
      newIndex = 0;
    } else if (newIndex < 0) {
      newIndex = playlists.length - 1;
    }
    setCurPlaylist(playlists[newIndex]);
  };
  return (
    <Card backgroundColor={bgColor} className='card-shadow mt-4'>
      <Container>
        <Row>
          <Col className='col-md-8 offset-md-2'>
            <Row className='no-gutters'>
              <div className='col-1 col-md-2 align-self-center'>
                <div
                  className='play-nav play-nav-left mr-3 mr-md-4'
                  onClick={() => nextPlaylist(true)}
                >
                  <img src={Icons.chevronRightPurple} alt='Left' />
                </div>
              </div>
              <div className='col-10 col-md-8'>
                {curPlaylist && getImage(curPlaylist)}
              </div>
              <div className='col-1 col-md-2 align-self-center'>
                <div
                  className='play-nav ml-3 ml-md-4'
                  onClick={() => nextPlaylist()}
                >
                  <img src={Icons.chevronRightPurple} alt='Right' />
                </div>
              </div>
            </Row>
            <Row>
              <Col className='col-sm-8 offset-sm-2'>
                <H3 textAlign='center'>{curPlaylist && curPlaylist.name}</H3>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </Card>
  );
};

export default withAuth(Playlist);
