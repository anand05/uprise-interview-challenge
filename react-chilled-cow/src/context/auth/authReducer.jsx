import { GET_TOKEN, SET_TOKEN, DEL_TOKEN } from '../types';

export default (state, action) => {
  const { type, payload, his } = action;
  switch (type) {
    case GET_TOKEN:
      return localStorage.getItem('up_cl_access_token');
    case SET_TOKEN:
      localStorage.setItem('up_cl_access_token', payload);
      return payload;
    case DEL_TOKEN:
      localStorage.removeItem('up_cl_access_token');
      return his.push('/connect');
    default:
      return state;
  }
};
