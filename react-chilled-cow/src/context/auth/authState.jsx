import React, { useReducer } from 'react';

import AuthContext from './authContext';
import AuthReducer from './authReducer';

import { GET_TOKEN, SET_TOKEN, DEL_TOKEN } from '../types';
import { useHistory } from 'react-router-dom';

const AuthState = (props) => {
  const his = useHistory();
  const [state, dispatch] = useReducer(
    AuthReducer,
    localStorage.getItem('up_cl_access_token')
  );

  const getToken = () => {
    dispatch({ type: GET_TOKEN });
  };

  const deleteToken = () => {
    dispatch({ type: DEL_TOKEN, his });
  };

  const setToken = (token) => {
    dispatch({ type: SET_TOKEN, payload: token });
  };

  return (
    <AuthContext.Provider
      value={{ auth: state, isAuth: !!state, getToken, deleteToken, setToken }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
