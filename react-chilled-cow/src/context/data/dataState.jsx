import React, { useReducer } from 'react';

import DataContext from './dataContext';
import DataReducer from './dataReducer';

import { GET_DATA, SET_DATA, DEL_DATA, FEAT_ARTS } from '../types';

const AuthState = (props) => {
  const [state, dispatch] = useReducer(DataReducer, { randomArts: [] });

  const getData = () => {
    dispatch({ type: GET_DATA });
    getTenArtists();
  };

  const setData = (data) => {
    return dispatch({ type: SET_DATA, payload: data });
  };

  const delData = (data) => {
    dispatch({ type: DEL_DATA, payload: data });
  };

  const getTenArtists = () => {
    dispatch({ type: FEAT_ARTS });
  };

  return (
    <DataContext.Provider
      value={{ data: state, getData, setData, delData, getTenArtists }}
    >
      {props.children}
    </DataContext.Provider>
  );
};

export default AuthState;
