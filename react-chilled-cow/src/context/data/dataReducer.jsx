import { GET_DATA, SET_DATA, DEL_DATA, FEAT_ARTS } from '../types';

const getRandom = (arr, n) => {
  if (!arr) {
    return [];
  }
  let result = new Array(n),
    len = arr.length,
    taken = new Array(len);
  if (n > len) {
    throw new RangeError('getRandom: more elements taken than available');
  }
  while (n--) {
    const x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
};

export default (state, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_DATA:
      const obStr = localStorage.getItem('up_cl_data');
      return obStr ? JSON.parse(obStr) : null;
    case SET_DATA:
      const { playlists: pls = [] } = payload,
        artists = new Set();

      pls.forEach((v) => {
        v.tracks.forEach((t) => {
          const sta = t.track.artists;
          sta.forEach((a) => {
            artists.add(a.name);
          });
        });
      });
      payload.artists = [...artists];
      localStorage.setItem('up_cl_data', JSON.stringify(payload));
      return payload;
    case DEL_DATA:
      localStorage.removeItem('up_cl_data');
      break;
    case FEAT_ARTS:
      const randomArts = getRandom(state.artists, 10);
      return { ...state, randomArts };
    default:
      return state;
  }
};
