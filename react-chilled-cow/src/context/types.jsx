export const GET_TOKEN = 0,
  DEL_TOKEN = 1,
  SET_TOKEN = 2;

export const GET_DATA = 0,
  SET_DATA = 1,
  DEL_DATA = 2,
  FEAT_ARTS = 3;
