import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { Row, Col, Container } from '@uprise/grid';
import { Card } from '@uprise/card';
import dataContext from 'context/data/dataContext';

const Header = ({ bgColor }) => {
  const { getTenArtists } = useContext(dataContext);

  const links = [
    { link: '/overview', label: 'Overview' },
    { link: '/playlist', label: 'Playlist' },
    {
      link: '/featured',
      label: 'Featured',
      classes: 'px-sm-0',
      onClick() {
        return getTenArtists;
      },
    },
  ];

  return (
    <Card backgroundColor={bgColor} className='card-shadow'>
      <Container>
        <Row className='justify-content-start'>
          {links.map(({ link, label, classes = '', onClick = () => {} }) => (
            <Col key={link} className={`col-sm-2 col-lg-1 ${classes}`}>
              <NavLink
                className='nav-link'
                activeClassName='nav-active'
                to={link}
                onClick={onClick()}
              >
                {label}
              </NavLink>
            </Col>
          ))}
        </Row>
      </Container>
    </Card>
  );
};

export default Header;
