import React from 'react';
import { Redirect } from 'react-router-dom';

const withAuth = (Component, connectComp = false) => {
  const AuthRoute = () => {
    const isAuth = !!localStorage.getItem('up_cl_access_token');
    if (isAuth) {
      if (connectComp) {
        return <Redirect to='/overview' />;
      }
      return <Component />;
    }
    if (!connectComp) {
      return <Redirect to='/connect' />;
    }
    return <Component />;
  };

  return AuthRoute;
};

export default withAuth;
