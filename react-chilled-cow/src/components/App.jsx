import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { Container } from '@uprise/grid';
import { backgrounds } from '@uprise/colors';
import { createGlobalStyle } from 'styled-components';

import '../App.scss';
import Featured from '../pages/Featured';
import Overview from '../pages/Overview';
import Playlist from '../pages/Playlist';
import Header from './Header';
import Connect from 'pages/Connect';
import AuthState from 'context/auth/authState';
import DataState from 'context/data/dataState';

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${backgrounds.fadedPurple};
  }
`;

const App = () => {
  const bgColor = backgrounds.white;

  return (
    <AuthState>
      <DataState>
        <Router>
          <GlobalStyle />
          <Container>
            <Header bgColor={bgColor} />
            <Switch>
              <Route exact path='/'>
                <Redirect to='/overview' />
              </Route>
              <Route exact path='/overview'>
                <Overview bgColor={bgColor} />
              </Route>
              <Route exact path='/playlist'>
                <Playlist bgColor={bgColor} />
              </Route>
              <Route exact path='/featured'>
                <Featured bgColor={bgColor} />
              </Route>
              <Route exact path='/connect'>
                <Connect />
              </Route>
            </Switch>
          </Container>
        </Router>
      </DataState>
    </AuthState>
  );
};

export default App;
