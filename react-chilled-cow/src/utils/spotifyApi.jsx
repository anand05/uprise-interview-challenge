import { SpotifyGraphQLClient } from 'spotify-graphql';
import config from '../config.json';

const getData = (userId = 'Chilledcow') => {
  const token = localStorage.getItem('up_cl_access_token');
  return SpotifyGraphQLClient({ ...config, accessToken: token })
    .query(
      `
      {
        user(id: "${userId}") {
          display_name
          images{
            height
            url
            width
          }
          uri
          href
          playlists{
            id
            description
            name
            tracks{
              track{
                artists{name}
              }
            }
            images{
              height
              width
              url
            }
            uri
            href
          }
        }
      }
`
    )
    .then((d) => {
      if (d.errors) {
        localStorage.removeItem('up_cl_access_token');
      }
      return d;
    })
    .catch((error) => {
      console.log(error);

      return error;
    });
};

export { getData };
